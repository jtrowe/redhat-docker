tag=rhd

all : .image

.image : Dockerfile
	docker build --file $< --tag rhd .

.container : .image
	docker run --detach --publish 80:80 --rm rhd > $@

run : .container

stop :
	docker stop $(shell cat .container)
	rm .container

