
# Actual UBI that works.
# But not registered to subscription manager. can't get access to repos.
#FROM registry.access.redhat.com/ubi8/ubi:latest

# Use CentOS for development w/o subscription
FROM centos:8

RUN yum install -y httpd; yum clean all
RUN echo "Hello World!" > /var/www/html/index.html

EXPOSE 80
CMD /usr/sbin/httpd -X
